﻿namespace Slmm.Client.Models
{
    public enum Command
    {
        invalid,
        quit,
        help,
        deploy,
        map,
        ping,
        w,
        a,
        s,
        d
    }
}