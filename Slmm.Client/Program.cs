﻿using Slmm.Client.IO;
using Slmm.Client.Models;
using Slmm.Client.Services;
using System;

namespace Client
{
    class Program
    {
        private static int Width;
        private static int Height;
        private static int MowerX;
        private static int MowerY;
        private static LawnStatus LawnSvc;

        static void Main(string[] args)
        {
            var lawnClient = new LawnClient(Slmm.Client.Properties.Settings.Default.SlmmServerApiEndpoint);
            LawnSvc = new LawnStatus(lawnClient);

            Command cmd;
            Menu.DrawMenu(false);
            Console.Write("> ");
            while ((cmd = Menu.ReadCommand(ref Width, ref Height, ref MowerX, ref MowerY)) != Command.quit)
            {
                var isDeployed = LawnSvc.GetStatus() != null;
                switch (cmd)
                {
                    case Command.quit:
                        Environment.Exit(0);
                        return;
                    case Command.help:
                        Menu.DrawMenu(isDeployed);
                        break;
                    case Command.deploy:
                        Deploy(isDeployed);
                        break;
                    case Command.map:
                        Map(isDeployed);
                        break;
                    case Command.ping:
                        Ping(isDeployed);
                        break;
                    case Command.w:
                        Move(isDeployed, Direction.Up);
                        break;
                    case Command.a:
                        Move(isDeployed, Direction.Left);
                        break;
                    case Command.s:
                        Move(isDeployed, Direction.Down);
                        break;
                    case Command.d:
                        Move(isDeployed, Direction.Right);
                        break;
                }

                Console.Write("> ");
            }
        }

        private static void Deploy(bool isDeployed)
        {
            if (isDeployed)
            {
                Console.WriteLine("Mower already deployed");
                return;
            }

            if (Width < 1 || Height < 1)
            {
                Console.WriteLine("The lawn must be bigger than a square of 1x1");
                return;
            }
            if (MowerX < 0 || MowerX >= Width || MowerY < 0 || MowerY >= Height)
            {
                Console.WriteLine("The initial position of the mower must be enclosed in the lawn area");
                return;
            }

            LawnSvc.SetLawnSize(Width, Height);
            LawnSvc.SetMowerInitialPosition(MowerX, MowerY);
            LawnSvc.Deploy();
            Console.WriteLine($"Mower initialized ({Width},{Height}), and deployed on the coordinates ({MowerX},{MowerY})");
        }

        private static void Map(bool isDeployed)
        {
            if (!isDeployed)
            {
                Console.WriteLine("Mower is not deployed");
                return;
            }

            Drawing.DrawGrid(LawnSvc.GetStatus(), MowerX, MowerY);
        }

        private static void Ping(bool isDeployed)
        {
            if (!isDeployed)
            {
                Console.WriteLine("Mower is not deployed");
                return;
            }

            var ping = LawnSvc.Ping();
            MowerX = ping.MowerX;
            MowerY = ping.MowerY;
            Console.WriteLine($"Status: Lawn ({ping.LawnWidth},{ping.LawnHeight}), Mower ({ping.MowerX},{ping.MowerY})");
        }

        private static void Move(bool isDeployed, Direction direction)
        {
            if (!isDeployed)
            {
                Console.WriteLine("Mower is not deployed");
                return;
            }

            var ping = LawnSvc.Move(direction);
            MowerX = ping.MowerX;
            MowerY = ping.MowerY;
            Console.WriteLine($"Status: Lawn ({ping.LawnWidth},{ping.LawnHeight}), Mower ({ping.MowerX},{ping.MowerY})");
        }
    }
}
