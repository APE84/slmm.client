﻿using Slmm.Client.Models;
using System;

namespace Slmm.Client.IO
{
    public static class Menu
    {
        public static void DrawMenu(bool isDeployed)
        {
            var status = isDeployed ? "Deployed" : "Waiting";
            Console.WriteLine("Status: " + status);
            Console.WriteLine("Commands:");
            Console.WriteLine("\t[deploy ? ? ? ?] Deploy mower into the lawn");
            Console.WriteLine("\t Where '?' are integers, in this order:");
            Console.WriteLine("\t  Lawn Width, Lawn Height, Mower X, Mower Y");
            Console.WriteLine("\t[w] Move up");
            Console.WriteLine("\t[a] Move left");
            Console.WriteLine("\t[s] Move down");
            Console.WriteLine("\t[d] Move right");
            Console.WriteLine("\t[map] Draw status map");
            Console.WriteLine("\t[ping] Get basic info");
            Console.WriteLine("\t[help] Show this menu");
            Console.WriteLine("\t[quit] Quit");
        }

        public static Command ReadCommand(ref int lawnWidth, ref int lawnHeight, ref int mowerX, ref int mowerY)
        {
            var line = Console.ReadLine().Trim();
            var value = string.Empty;
            if (line.ToLower().StartsWith("deploy "))
            {
                var args = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (args.Length != 5)
                {
                    Console.WriteLine("Deploy command must follow the pattern: \"deploy [int] [int] [int] [int]\"");
                    return Command.invalid;
                }

                if (!int.TryParse(args[1], out lawnWidth))
                {
                    Console.WriteLine($"Lawn Width must be a valid integer, found \"{args[1]}\"");
                    return Command.invalid;
                }

                if (!int.TryParse(args[2], out lawnHeight))
                {
                    Console.WriteLine($"Lawn Height must be a valid integer, found \"{args[2]}\"");
                    return Command.invalid;
                }

                if (!int.TryParse(args[3], out mowerX))
                {
                    Console.WriteLine($"Mower X must be a valid integer, found \"{args[3]}\"");
                    return Command.invalid;
                }

                if (!int.TryParse(args[4], out mowerY))
                {
                    Console.WriteLine($"Mower Y must be a valid integer, found \"{args[4]}\"");
                    return Command.invalid;
                }

                return Command.deploy;
            }

            Command cmd;
            if (!Enum.TryParse(line, true, out cmd))
            {
                cmd = Command.invalid;
                Console.WriteLine("Unrecognised command");
            }

            return cmd;
        }
    }
}
