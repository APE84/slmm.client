﻿using System;

namespace Slmm.Client.IO
{
    public static class Drawing
    {
        private const int BufferOffset = 3;
        private const int WindowOffset = 10;
        private const char MowedSymbol = '.';
        private const char GrassSymbol = '!';
        private const char CurrentPositionSymbol = 'O';

        public static void DrawGrid(bool[,] grid, int currenX, int currentY)
        {
            var cursorLeft = Console.CursorLeft;
            var cursorTop = Console.CursorTop;

            var maxWidth = grid.GetUpperBound(0);
            var maxHeight = grid.GetUpperBound(1);

            Console.Clear();
            if (Console.BufferWidth < maxWidth + BufferOffset || Console.BufferHeight < maxHeight + BufferOffset)
            {
                Console.SetBufferSize(maxWidth + BufferOffset, maxHeight + BufferOffset);
            }
            if (Console.WindowWidth < maxWidth + WindowOffset || Console.WindowHeight < maxHeight + WindowOffset)
            {
                Console.SetWindowSize(maxWidth + WindowOffset, maxHeight + WindowOffset);
            }

            for (int i = 0; i <= maxWidth; i++)
            {
                for (int j = maxHeight; j >= 0; j--)
                {
                    var symbol = GrassSymbol;
                    if (i == currenX && j == currentY)
                    {
                        symbol = CurrentPositionSymbol;
                    }
                    else if (grid[i, j])
                    {
                        symbol = MowedSymbol;
                    }
                    Console.CursorLeft = i + 1;
                    Console.CursorTop = maxHeight - j + 1;
                    Console.Write(symbol);
                }
            }

            Console.CursorLeft = 0;
            Console.CursorTop = maxHeight + BufferOffset;
        }
    }
}
