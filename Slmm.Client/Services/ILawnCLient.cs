﻿using Slmm.Client.Models;

namespace Slmm.Client.Services
{
    public interface ILawnCLient
    {
        Garden Ping();

        Garden Move(Direction direction);

        void Deploy(int lawnWidth, int lawnHeight, int mowerStartPositionX, int mowerStartPositionY);
    }
}
