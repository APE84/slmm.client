﻿using Slmm.Client.Models;
using System;

namespace Slmm.Client.Services
{
    public class LawnStatus
    {
        private static int Width;
        private static int Height;
        private static int MowerX;
        private static int MowerY;

        private static bool[,] StatusGrid;

        private ILawnCLient LawnApi;

        private static bool LawnInitialized = false;

        public LawnStatus(ILawnCLient lawnClient)
        {
            Width = 10;
            Height = 10;

            MowerX = 0;
            MowerY = 0;

            LawnApi = lawnClient;
        }

        public void SetLawnSize(int width, int height)
        {
            if (LawnInitialized)
            {
                throw new InvalidOperationException("Mower already deployed");
            }

            if (width < 1 || height < 1)
            {
                throw new ArgumentOutOfRangeException("The lawn must be bigger than a square of 1x1");
            }

            Width = width;
            Height = height;
        }

        public void SetMowerInitialPosition(int x, int y)
        {
            if (LawnInitialized)
            {
                throw new InvalidOperationException("Mower already deployed");
            }

            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                throw new ArgumentOutOfRangeException("The initial position of the mower must be enclosed in the lawn area");
            }

            MowerX = x;
            MowerY = y;
        }

        public void InitializeFromGarden()
        {
            var garden = LawnApi.Ping();

            if (garden.LawnWidth < 1 || garden.LawnHeight < 1)
            {
                throw new InvalidOperationException("The lawn must be bigger than a square of 1x1");
            }

            if (garden.MowerX < 0 || garden.MowerX >= garden.LawnWidth || garden.MowerY < 0 || garden.MowerY >= garden.LawnHeight)
            {
                throw new InvalidOperationException("The position of the mower must be enclosed in the lawn area");
            }

            Width = garden.LawnWidth;
            Height = garden.LawnHeight;
            MowerX = garden.MowerX;
            MowerY = garden.MowerY;

            LawnInitialized = true;
            FillStatus();
        }

        public void Deploy()
        {
            if (LawnInitialized)
            {
                throw new InvalidOperationException("Mower already deployed");
            }

            LawnApi.Deploy(Width, Height, MowerX, MowerY);
            LawnInitialized = true;
            FillStatus();
        }

        public Garden Ping()
        {
            if (!LawnInitialized)
            {
                throw new InvalidOperationException("Mower not deployed");
            }

            return LawnApi.Ping();
        }

        public Garden Move(Direction direction)
        {
            if (!LawnInitialized)
            {
                throw new InvalidOperationException("Mower not deployed");
            }

            var garden = LawnApi.Move(direction);
            StatusGrid[garden.MowerX, garden.MowerY] = true;

            return garden;
        }

        public bool[,] GetStatus()
        {
            return StatusGrid;
        }

        private void FillStatus()
        {
            if (StatusGrid != null)
            {
                throw new InvalidOperationException("Can't reinitialize the status grid");
            }
            if (!LawnInitialized)
            {
                throw new InvalidOperationException("Lawn is not initialized");
            }

            StatusGrid = new bool[Width, Height];

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    StatusGrid[i, j] = i == MowerX && j == MowerY;
                }
            }
        }
    }
}
