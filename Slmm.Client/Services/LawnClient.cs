﻿using Newtonsoft.Json;
using RestSharp;
using Slmm.Client.Models;
using System;
using System.Net;

namespace Slmm.Client.Services
{
    public class LawnClient : ILawnCLient
    {
        private readonly string BaseUrl;
        private const string LawnResourceUrl = "lawn";
        private const string MowResourceUrl = "lawn/mow";

        public LawnClient(string endpointBaseUrl)
        {
            BaseUrl = endpointBaseUrl;
        }

        public Garden Ping()
        {
            var request = BuildRequest(Method.GET, LawnResourceUrl);
            var response = CallApi(request);

            ValidateCallStatusOK(response, "Lawn Ping");

            var garden = JsonConvert.DeserializeObject<Garden>(response.Content);
            return garden;
        }

        public Garden Move(Direction direction)
        {
            var mow = new Mow() { Move = direction };
            var request = BuildRequest(Method.POST, MowResourceUrl, mow);
            var response = CallApi(request);

            ValidateCallStatusOK(response, "Mower Move");

            var garden = JsonConvert.DeserializeObject<Garden>(response.Content);
            return garden;
        }

        public void Deploy (int lawnWidth, int lawnHeight, int mowerStartPositionX, int mowerStartPositionY)
        {
            if (lawnWidth < 1 || lawnHeight < 1)
            {
                throw new ArgumentOutOfRangeException("The lawn must be bigger than a square of 1x1");
            }
            if (mowerStartPositionX < 0 || mowerStartPositionX >= lawnWidth || mowerStartPositionY < 0 || mowerStartPositionY >= lawnHeight)
            {
                throw new ArgumentOutOfRangeException("The initial position of the mower must be enclosed in the lawn area");
            }

            var garden = new Garden()
            {
                LawnWidth = lawnWidth,
                LawnHeight = lawnHeight,
                MowerX = mowerStartPositionX,
                MowerY = mowerStartPositionY
            };

            var request = BuildRequest(Method.POST, LawnResourceUrl, garden);
            var response = CallApi(request);
            ValidateCallStatusOK(response, "Lawn Deploy");
        }

        private void ValidateCallStatusOK(IRestResponse response, string actionCalled)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                if (response.ErrorException != null)
                {
                    throw response.ErrorException;
                }
                else
                {
                    throw new InvalidOperationException($"SLMM Mower responded to '{actionCalled}', with '{response.StatusCode}' HTTP code");
                }
            }
        }

        private IRestResponse CallApi(IRestRequest request)
        {
            var client = new RestClient(BaseUrl);
            return client.Execute(request);
        }

        private IRestRequest BuildRequest(Method method, string resource, object body=null)
        {
            var request = new RestRequest()
            {
                Method = method,
                Resource = resource
            };

            if (body != null)
            {
                request.AddJsonBody(body);
            }

            return request;
        }
    }
}
